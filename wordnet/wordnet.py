import nltk 
from nltk.corpus import wordnet 

#from pprint import pprint

import pandas as pd  
from pandas import DataFrame

import csv


df = pd.read_csv('../wordnet/data/hashtag_frequency.csv')
hashtags = df['hashtags']
freq = df['frequency']


zipped = zip(hashtags, freq)
h_f_dict = dict(zipped)


def get_synonyms(hashtags):
	expansions = {}
	for h in hashtags:
		expansions[h] = []
		for syn in wordnet.synsets(str(h), lang='swe'):
			for l in syn.lemmas():
				expansions[h].append(l.name())
	return expansions


expansions = get_synonyms(hashtags)
 
with open('../wordnet/result/results.csv', 'w') as f:
	w = csv.writer(f)
	w.writerows(expansions.items())
