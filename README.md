## How to run the tool:

## Word2Vec
#### Python 3 must be installed
Install the following libraries:

```shell
pip install gensim nltk matplotlib numpy scipy scikit-learn
```
#### Directory:
- data: in the subdirectory "raw", there are original tweet data sets.
in the subdirectory "tiddy", a csv file for hashtags and the corresponding frequency and another csv file for original data sorted by hashtag.
- pretrained_models: the trained models will be saved in this directory when running train.py
- results: the results of hashtag expansions are saved in the subdirectory "cbow_expansion" and the subdirectory "sg_expansion".
In the subdirectory "csv_for_next_project/frequency_50", the results are manually labeled. This file is used for calculating precision. This result is based on the expansion of hashtags (that occur more than 50 times in the original data set ).

#### Running:
- Change to directory "word2vec"
- Write the following line on terminal:
  python3 train.py "algorithm_you_want_to_test" "dimensionality_size" "number_of_iteration"
  For skip-gram algorithm, write sg. For continuous bag of words, write cbow.
- Example usage: 
```shell
python3 train.py cbow 100 5
```
```shell
python3 train.py sg 200 20
```
- After the training, run evaluation.py in order to get the hashtag expansions.
- Write the following line on terminal:
python3 evaluation.py "algorithm" "number of expansions"
- Example usage:
```shell
python3 evaluation cbow 5
```
```shell
python3 evaluation.py sg 10
```
- In order to calculate precision of results:
```shell
python3 precision.py
```
- In order to get Jaccard index between 5 expansions and 10 expansions, write the following line on the terminal:
python3 jaccard.py "algorithm"

- Example usage:
```shell
python3 jaccard.py cbow
```
```shell
python3 jaccard.py sg
```

## Twitter specific method
#### Python 3 must be installed
Install the following libraries:

```shell
pip install nltk matplotlib numpy scipy scikit-learn
```
#### Directory:
- data: in the subdirectory "raw", there are original tweet data sets.
in the subdirectory "tiddy", there are .json files that are created during process. These files are used for clustering.
- results: the subdirectory "clusters" contains the result of clustering

#### Running:
- Change to directory "twitter"

-Step1: preprocessing
```shell
python3 preprocess.py
```
-Step2: build unigram of hashtags
```shell
python3 unigram_hashtags.py
```
-Step3: build occurrences of all words unigram 
```shell
python3 unigram_words.py
```
-Step4: build hashtag features
```shell
python3 hashtag_features.py
```
-Step5: build hashtag-words features
```shell
python3 word_features.py
```
-Step6: Clustering
```shell
python3 clustering.py
```

## WordNet
#### Python 3 must be installed
Install the following libraries:

```shell
pip install nltk
```
#### Directory:
- data: there is an original tweet data set.
- result: the result is saved in this directory

#### Running:
- Change to directory "wordnet"
- Write the following line on terminal:

```shell
python3 wordnet.py
```


