# Step 3: build hashtag-hashtag features #


import json
import os
import operator

def main():

	filename = '../twitter/data/tiddy/all_hashtags.json'
	
	hashtag_objects = []
	all_hashtags = []

	with open(filename, 'r') as f:
		for idx, line in enumerate(f):
			data = json.loads(line)
			num_post = int(data['num_post'])
			if num_post <= 3:
				continue

			hashtag_objects.append(data)
			all_hashtags.append(data['hashtag'])

	all_hashtags.sort()

	with open('../twitter/data/tiddy/hashtag_features_with_cooccur_{0:d}_list.json'.format(len(all_hashtags)), 'w') as output:
		output.write('{0}\n'.format(
			json.dumps({'hashtags': all_hashtags})
			)
		)

	with open('../twitter/data/tiddy/hashtag_features_with_cooccur_{0:d}.json'.format(len(all_hashtags)), 'w') as output:
		for hashtag_obj in hashtag_objects:
			other_hashtags = dict()
			for h in all_hashtags:
				other_hashtags[h] = 0

			for h in hashtag_obj['other_hashtags'].keys():
				if h in other_hashtags:
					other_hashtags[h] = hashtag_obj['other_hashtags'][h]

			features = []
			for h in all_hashtags:
				features.append(other_hashtags[h])

			output.write('{0}\n'.format(
				json.dumps({'hashtag': hashtag_obj['hashtag'], 'features': features})
				)
			)


if __name__ == '__main__':
	main()