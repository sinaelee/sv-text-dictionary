#!/usr/bin/env python
# -*- coding: utf-8 -*-


import json

import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from stop_words import get_stop_words
from nltk.stem.snowball import SnowballStemmer
from nltk.stem import PorterStemmer

import pandas as pd  
from pandas import DataFrame

from pprint import pprint

import csv
import re
import os



punctuation_tokens = ['.', '..', '...', ',', ';', ':', '(', ')', '"', '\'', '[', ']',
                      '{', '}', '<', '>', '?', '!', '-', '_', '+', '*', '--', '__', '\'\'', '``', '@', '#', '+', '&', '—', '’', '…', '”', '|', '/', '–', '=']


def make_sentences(tweet):
    swedish_tokenizer = nltk.data.load('tokenizers/punkt/PY3/swedish.pickle')
    sents = swedish_tokenizer.tokenize(tweet) 
    return sents

# make words given sentences: output är list of list [['word', 'word', 'word', ...], ['word', 'word', ...], ...]
def make_words(flattened_l):
    tokenized_sents = [word_tokenize(f) for f in flattened_l]
    return tokenized_sents


# Make Sweish stopwords 
def make_sw_stop_words():
    stop_words = get_stop_words('swedish')  # about 900 stopwords
    nltk_stop_words = stopwords.words('swedish') # about 150 words
    stop_words.extend(nltk_stop_words)
    stop_words.extend(['ska', 'vill', 'måste', 'får', 'få', 'fick', 'fått', 'via', 'expressen', 'expressentv', 'expressenial=twtr'])
    return stop_words

# Make English stopwords
def make_en_stop_words():
    stop_words_eng = get_stop_words('en')
    nltk_stop_words_eng = stopwords.words('english')
    stop_words_eng.extend(nltk_stop_words_eng)
    return stop_words_eng

def preprocess(tweets):
    # Remove the retweet mention 
    mention_removed = [re.sub(r'@\w+', '', tweet) for tweet in tweets] 

    hashtag_removed = [re.sub(r'#\w+', '', tweet) for tweet in mention_removed] 

    url_removed = [re.sub(r'http\S+|https\S+|www\S+', '', tweet) for tweet in hashtag_removed]

    # Remove pic.twitter.com/blabla
    picture_removed = [re.sub(r'pic\.twitter\.com\S+', '', tweet) for tweet in url_removed]

    weird_removed = [re.sub(r'\w+\.\w+\.\S+', '', tweet) for tweet in url_removed]  #com_removed

    instagram_removed = [re.sub(r'\.instagram\.com\/\S+', '', tweet) for tweet in weird_removed]

    # remove broken url from the raw data
    broken_removed = [re.sub(r'\w+\.\w+\/\S+', '', tweet) for tweet in instagram_removed]

    #broken_removed = [re.sub(r'\w+\/\w+\/\S+', '', sentence) for sentence in broken_removed]

    # Remove the weird url 
    com_removed = [re.sub(r'\.com\/\S+', '', tweet) for tweet in broken_removed]

    # Remove unicode characters
    unicode_removed = [re.sub(r'\<[uU]\+[a-zA-Z]*[0-9]+[a-zA-Z]*[0-9]*[a-zA-Z]*\>', '', tweet) for tweet in com_removed]

    # Remove hexdump command
    hexdump_removed = [re.sub(r'\<[fF]+[0-9]+\>', '', tweet) for tweet in unicode_removed]

    # Remove the rest of unicode containing '<characters>m' or '< >'
    rest_removed = [re.sub(r'\<[a-zA-Z]*[0-9]*\>', '', tweet) for tweet in hexdump_removed]

    emoji_removed = [re.sub(r'\<U\S+\>', '', tweet) for tweet in rest_removed]


    # Remove weird ellipsis following after a character or string. Ex: än… => än
    ellipsis_removed = [re.sub(r'\…', '', tweet) for tweet in emoji_removed]


    # Remove text characters of HTML. Ex: &gt, &lt, &amp 
    html_removed = [re.sub(r'\&[gtampl]+', '', tweet) for tweet in ellipsis_removed]

    # Remove hyphen between strings. Ex prao-elev => prao, elev
    hyphen_removed = [re.sub(r'\-', '', tweet) for tweet in html_removed]

    # Remove plus between strings. Ex s+mp => s, mp
    plus_removed = [re.sub(r'\+', '', tweet) for tweet in hyphen_removed]

    # Remove slash between strings. Ex. rattfylleri/olovlig => rattfylleri olovlig
    slash_removed = [re.sub(r'\/', '', tweet) for tweet in plus_removed]

    # Remove empty lists
    #words = [w for w in words if w != []]

    return slash_removed


def main():
    df = pd.read_csv('../twitter/data/raw/hashtags_only.csv')
    org_hashtags = df['hashtags']
    org_tweets = df['text']
    unicode_removed = [re.sub(r'\<u\+*[0-9]+[a-zA-Z]*[0-9]*[a-z]*\>', '', h) for h in org_hashtags]
    newDf = pd.DataFrame({'hashtags': unicode_removed, 'tweets': org_tweets})
    newDf.to_csv('../twitter/data/raw/hashtags_tweets.csv')

    df1 = pd.read_csv('../twitter/data/raw/hashtags_tweets.csv')
    hashtags = df1['hashtags']
    tweets = df1['tweets']
    hashtag_lst = list(hashtags)
    #pprint(hashtag_lst)
    tweet_lst = list(tweets)
    text = preprocess(tweets)
    #pprint(tmp_text)
    zipped = zip(hashtag_lst, text)
    #pprint(list(zipped))
    zipped_lst = list(zipped)
    #pprint(zipped_lst)
    stop_words = make_sw_stop_words()
    stemmer = SnowballStemmer('swedish')

    en_stop_words = make_en_stop_words()
    en_stemmer = PorterStemmer()

    filename = '../twitter/data/tiddy/hashtag_and_tweet.json'
    with open(filename, 'w') as output:
        for fst, snd in zipped_lst:
            hashtag = fst.split()
            #print(hashtag)
            sentences = make_sentences(snd)
            words = make_words(sentences)
            flatten_words = [w for sublist in words for w in sublist]
            cleaned = [w for w in flatten_words if w not in punctuation_tokens]
            cleaned = [w for w in cleaned if not w.isdigit()]
            cleaned = [w for w in cleaned if w not in stop_words]
            cleaned = [stemmer.stem(w) for w in cleaned]
            cleaned = [stemmer.stem(w) for w in cleaned]
            cleaned = [en_stemmer.stem(w) for w in cleaned]
            output.write('{0}\n'.format(
                json.dumps({'hashtags': hashtag, 'tweet': cleaned})))

if __name__ == '__main__':
    main()
