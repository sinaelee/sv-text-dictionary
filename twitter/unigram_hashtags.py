#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Step 2: build unigram array for hashtags

import json
import os

def main():
	filename = '../twitter/data/tiddy/hashtag_and_tweet.json'

	all_hashtags = dict()

	with open(filename, 'r') as f:
		print ('Process file {0}...'.format(filename))

		for idx, line in enumerate(f):
			if (idx+1)%1000 == 0:
				print ('== Process record {0:d}...'.format(idx+1))

			data = json.loads(line)

			hashtags = data['hashtags']
			text = data['tweet']

			for hashtag in hashtags:
				if hashtag not in all_hashtags.keys():
					all_hashtags[hashtag] = [dict(), dict(), 1]
				else:
					all_hashtags[hashtag][2] += 1

				other_hashtags = list(set(hashtags) - set([hashtag]))

				for other_hashtag in other_hashtags:
					if other_hashtag not in all_hashtags[hashtag][1].keys():
						all_hashtags[hashtag][1][other_hashtag] = 1
					else:
						all_hashtags[hashtag][1][other_hashtag] += 1


			for word in text:
				for hashtag in hashtags:
					if word not in all_hashtags[hashtag][0].keys():
						all_hashtags[hashtag][0][word] = 1
					else:
						all_hashtags[hashtag][0][word] += 1

	with open('../twitter/data/tiddy/all_hashtags.json', 'w') as output:
		for hashtag in all_hashtags.keys():
			output.write('{0}\n'.format(
				json.dumps(
					{'hashtag': hashtag, 'num_post': all_hashtags[hashtag][2], 
					'words': all_hashtags[hashtag][0], 'other_hashtags': all_hashtags[hashtag][1]}, 
					sort_keys=True)
				)
			)


if __name__ == '__main__':
	main()