import sys
import os
import pandas as pd
from pprint import pprint
import json  
import csv


def main():
    '''
    filename = '../twitter/results/clusters/1_rst_all_hashtags.txt'

    df = pd.read_csv(filename, delimiter='\t')
    df.columns = ['index', 'expansions', 'cluster_id']
    df.to_csv('test.csv', index=False)

    newDf = pd.read_csv('test.csv')
    expansions = newDf['expansions']
    cluster_id = newDf['cluster_id']


    zipped = zip(expansions, cluster_id)
    my_dict = dict(zipped)

    labels = set(my_dict.values())

    sorted_after_labels = {}

    for l in labels:
        sorted_after_labels[l] = [k for k in my_dict.keys() if my_dict[k] == l]
    

    with open('sorted.csv', 'w') as f:
        column_n = ['labels', 'expansions']
        w = csv.DictWriter(f, fieldnames=column_n)
        w.writeheader()

        for k, v in sorted_after_labels.items():
            w.writerow({'labels': k, 'expansions': v})
    '''

    with open('../twitter/data/tiddy/all_hashtags.json', 'r') as f:
        data = json.load(f)
    
    with open('hf.csv', 'w') as csvfile:
        out = csv.writer(csvfile)
        for item in data:
            fields = list(item['other_hashtags'].values())
            wrds = list(item['words'].values())
            out.writerow([item['hashtag'], item['num_post']] + fields + wrds)




if __name__ == '__main__':
    main()
