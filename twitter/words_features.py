# Step 3: build hashtag-unigram features #

import json
import os
import operator
from pprint import pprint

def main():

    filename = '../twitter/data/tiddy/all_words.json'
    all_words_list = None

    with open(filename, 'r') as f:
        for idx, line in enumerate(f):
            all_words_list = json.loads(line)
            #pprint(all_words_list)
            #break

    len_all_words = len(all_words_list)
    first_ns = [len_all_words]
    
    for first_n in first_ns:
        all_words = dict()
        export_data = []

        for idx, word in enumerate(all_words_list):
            if idx >= first_n:
                break
            all_words[word[0]] = word[1]

        with open('../twitter/data/tiddy/all_hashtags.json', 'r') as f:
            for idx, line in enumerate(f):
                data = json.loads(line)
                num_post = int(data['num_post'])
                if num_post <= 3:
                    continue

                hashtag_text = data['hashtag']
                hashtag_words = data['words']

                # Remove words that are not in feature words list
                tmp_list = list(set(hashtag_words.keys()) - set(all_words.keys()))
                for tmp_word in tmp_list:
                    hashtag_words.pop(tmp_word, None)

                # Add feature words that the target hashtag did not have
                tmp_list = list(set(all_words.keys()) - set(hashtag_words.keys()))
                for tmp_word in tmp_list:
                    hashtag_words[tmp_word] = 0

                export_data.append([hashtag_text, hashtag_words])

        all_words_keys = list(all_words.keys())
        all_words_keys.sort()

        with open('../twitter/data/tiddy/hashtag_features_with_words_{0:d}_list.json'.format(first_n), 'w') as output:
            output.write('{0}\n'.format(
                json.dumps({'hashtags': all_words_keys})
                )
            )

        with open('../twitter/data/tiddy/hashtag_features_with_words_{0:d}.json'.format(first_n), 'w') as output:
            for data in export_data:
                features = []
                for word_key in all_words_keys:
                    features.append(data[1][word_key])
                output.write('{0}\n'.format(
                    json.dumps({'hashtag': data[0], 'features': features})
                    )
                )

        print("the number of hashtags: {0:d}, unigram features: {1:d}".format(len(export_data), first_n))


if __name__ == '__main__':
    main()




