
import pandas as pd  
from pandas import DataFrame

import csv
from pprint import pprint

from collections import defaultdict

import os
import sys
import ast


def topn_to_list(csv):
	df = pd.read_csv(csv)

	topn = df['topn']
	topn_to_lst = topn.values.tolist()
	topn_lst = [ast.literal_eval(elem) for elem in topn_to_lst]

	flat_list = [w for sublist in topn_lst for w in sublist]

	return flat_list


# Get topn similar words that frequency of hashtag is more than k
def topn_to_list_k(csv):
	df = pd.read_csv(csv)

	topn = df['topn']
	topn_to_lst = topn.values.tolist()
	topn_lst = [ast.literal_eval(elem) for elem in topn_to_lst]

	freq = df['frequency']
	freq_to_lst = freq.values.tolist()

	pairs = zip(freq_to_lst, topn_lst)

	dictionary = {}

	for n, v in pairs:
		dictionary.setdefault(n, []).append(v)

	more_than_k_freq = [v for k, v in dictionary.items() if k > 50]

	list_of_lists = [w for sublist in more_than_k_freq for w in sublist]
	flat_list = [w for sublist in list_of_lists for w in sublist]

	return flat_list



def hashtags_to_list(csv):
	df = pd.read_csv(csv)
	hashtags = df['hashtags']
	hashtags_to_lst = hashtags.values.tolist()
	
	return hashtags_to_lst


def hashtags_to_list_k(csv):
	df = pd.read_csv(csv)
	hashtags = df['hashtags']
	hashtags_to_lst = hashtags.values.tolist()

	freq = df['frequency']
	freq_to_lst = freq.values.tolist()

	pairs = zip(freq_to_lst, hashtags_to_lst)

	dictionary = {}

	for n, v in pairs:
		dictionary.setdefault(n, []).append(v)

	more_than_k_freq = [v for k, v in dictionary.items() if k > 50]
	flat_list = [w for sublist in more_than_k_freq for w in sublist]

	return flat_list

# For whole 
def jaccard_similarity_for_each_model(csv):
	
	hashtags = hashtags_to_list(csv)
	set_hashtags = set(hashtags)
	
	topn = topn_to_list(csv)
	unique_topn = set(topn)

	intersection = list(set_hashtags.intersection(unique_topn))
	nr_intersection = len(intersection)
	

	union = list(set_hashtags.union(unique_topn))
	nr_union = len(union)

	return (float(nr_intersection) / nr_union)*100


# for more than k frequency
def k_jaccard_similarity_for_each_model(csv):

	hashtags = hashtags_to_list_k(csv)
	set_hashtags = set(hashtags)
	
	topn = topn_to_list_k(csv)
	unique_topn = set(topn)

	intersection = list(set_hashtags.intersection(unique_topn))
	nr_intersection = len(intersection)

	union = list(set_hashtags.union(unique_topn))
	nr_union = len(union)

	return (float(nr_intersection) / nr_union)*100


# jaccard similarity for two different models
def jaccard_similarity(csv1, csv2):
	topn1 = topn_to_list(csv1)
	unique_topn1 = set(topn1)

	topn2 = topn_to_list(csv2)
	unique_topn2 = set(topn2)

	intersection= list(unique_topn1.intersection(unique_topn2))
	nr_intersection = len(intersection)

	union = list(unique_topn1.union(unique_topn2))
	nr_union = len(union)
	return (float(nr_intersection) / nr_union)*100

# for more than k frequency only similar words  k=30
def k_jaccard_similarity(csv1, csv2):
	topn1 = topn_to_list_k(csv1)
	unique_topn1 = set(topn1)
	#print('len of unique topn1', len(unique_topn1))

	topn2 = topn_to_list_k(csv2)
	unique_topn2 = set(topn2)

	#print('len of unique topn2', len(unique_topn2))

	intersection= list(unique_topn1.intersection(unique_topn2))
	nr_intersection = len(intersection)
	#print(nr_intersection)

	union = list(unique_topn1.union(unique_topn2))
	nr_union = len(union) 
	#print(nr_union)
	return (float(nr_intersection) / nr_union)*100


# Usage: python3 jaccard.py cbows om man vill räkna jaccard similarity för två filer in cbow

cbow_in_dir = 'results/cbow_expansion'
sg_in_dir = 'results/sg_expansion'

if __name__ == "__main__":

	if sys.argv[1] == 'cbow':
		cbow_file_lst_for_5 = [f for f in os.listdir(cbow_in_dir) if f.endswith('5-similar_words.csv')]
		cbow_file_lst_for_5.sort()
		cbow_file_lst_for_5 = [os.path.join(cbow_in_dir, name) for name in cbow_file_lst_for_5]

		cbow_file_lst_for_10 = [f for f in os.listdir(cbow_in_dir) if f.endswith('10-similar_words.csv')]
		cbow_file_lst_for_10.sort()
		cbow_file_lst_for_10 = [os.path.join(cbow_in_dir, name) for name in cbow_file_lst_for_10]


		for five, ten in zip(cbow_file_lst_for_5, cbow_file_lst_for_10):
			file_name_for_top5 = os.path.splitext(os.path.basename(five))[0]
			file_name_for_top10 = os.path.splitext(os.path.basename(ten))[0]
			test = k_jaccard_similarity(five, ten)
			print(file_name_for_top5, file_name_for_top10, test)

	if sys.argv[1] == 'sg':
		sg_file_lst_for_5 = [f for f in os.listdir(sg_in_dir) if f.endswith('5-similar_words.csv')]
		sg_file_lst_for_5.sort()
		sg_file_lst_for_5 = [os.path.join(sg_in_dir, name) for name in sg_file_lst_for_5]

		sg_file_lst_for_10 = [f for f in os.listdir(sg_in_dir) if f.endswith('10-similar_words.csv')]
		sg_file_lst_for_10.sort()
		sg_file_lst_for_10 = [os.path.join(sg_in_dir, name) for name in sg_file_lst_for_10]


		for five, ten in zip(sg_file_lst_for_5, sg_file_lst_for_10):
			file_name_for_top5 = os.path.splitext(os.path.basename(five))[0]
			file_name_for_top10 = os.path.splitext(os.path.basename(ten))[0]
			test = k_jaccard_similarity(five, ten)
			print(file_name_for_top5, file_name_for_top10, test)

