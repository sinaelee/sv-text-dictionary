import pandas as pd  
from pandas import DataFrame

import os
import sys



def calculate_precision(src):
	df = pd.read_csv(src, delimiter=';')
	labels = df['label']
	#labels = labels.tolist()

	true_positives = 0
	false_positives = 0

	for label in labels:
		if label == 1:
			true_positives += 1

		if label == 0:
			false_positives += 1


	precision = true_positives/(true_positives+false_positives)
	precision = precision * 100

	return precision


in_dir = 'results/csv_for_next_project/frequency_50'

if __name__ == "__main__":
	file_lst = [f for f in os.listdir(in_dir) if f.endswith('5-similar_words.csv')]
	file_lst.sort()
	file_lst = [os.path.join(in_dir, name) for name in file_lst]

	for f in file_lst:
		file_name = os.path.splitext(os.path.basename(f))[0]
		precision = calculate_precision(f)
		print(file_name, 'Precision: ', precision)
