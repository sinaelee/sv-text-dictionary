#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from stop_words import get_stop_words
#from nltk.stem.snowball import SnowballStemmer
import re

import pandas as pd  
from pandas import DataFrame

import csv
from pprint import pprint



punctuation_tokens = ['.', '..', '...', ',', ';', ':', '(', ')', '"', '\'', '[', ']',
                      '{', '}', '<', '>', '?', '!', '-', '_', '+', '*', '--', '__', '\'\'', '``', '@', '#', '+', '&', '—', '’', '…', '”', '|', '/', '–', '=']


# make flattend list of sentences: ['sent', 'sent', 'sent', ...]    
def make_sentences(tweets):
    swedish_tokenizer = nltk.data.load('tokenizers/punkt/PY3/swedish.pickle')
    sents = [swedish_tokenizer.tokenize(t) for t in tweets]  # list of list
    flattened = []
    for sublist in sents:
        for s in sublist:
            flattened.append(s)
    return flattened

# make words given sentences: output är list of list [['word', 'word', 'word', ...], ['word', 'word', ...], ...]
def make_words(flattened_l):
    tokenized_sents = [word_tokenize(f) for f in flattened_l]
    return tokenized_sents


# Make Sweish stopwords 
def make_sw_stop_words():
    stop_words = get_stop_words('swedish')  # about 900 stopwords
    nltk_stop_words = stopwords.words('swedish') # about 150 words
    stop_words.extend(nltk_stop_words)
    stop_words.extend(['ska', 'vill', 'måste', 'får', 'få', 'fick', 'fått', 'via', 'expressen', 'expressentv', 'expressenial=twtr'])
    return stop_words

# Make English stopwords
def make_en_stop_words():
    stop_words_eng = get_stop_words('en')
    nltk_stop_words_eng = stopwords.words('english')
    stop_words_eng.extend(nltk_stop_words_eng)
    return stop_words_eng


def preprocess(tweets):
    sentences = make_sentences(tweets)

    # Remove the retweet mention 
    mention_removed = [re.sub(r'@\w+', '', sentence) for sentence in sentences] 

    url_removed = [re.sub(r'http\S+|https\S+|www\S+', '', sentence) for sentence in mention_removed]

    # Remove pic.twitter.com/blabla
    picture_removed = [re.sub(r'pic\.twitter\.com\S+', '', sentence) for sentence in url_removed]

    weird_removed = [re.sub(r'\w+\.\w+\.\S+', '', sentence) for sentence in url_removed]  #com_removed

    instagram_removed = [re.sub(r'\.instagram\.com\/\S+', '', sentence) for sentence in weird_removed]

    # remove broken url from the raw data
    broken_removed = [re.sub(r'\w+\.\w+\/\S+', '', sentence) for sentence in instagram_removed]

    #broken_removed = [re.sub(r'\w+\/\w+\/\S+', '', sentence) for sentence in broken_removed]

    # Remove the weird url 
    com_removed = [re.sub(r'\.com\/\S+', '', sentence) for sentence in broken_removed]



    # Remove unicode characters
    unicode_removed = [re.sub(r'\<[uU]\+[a-zA-Z]*[0-9]+[a-zA-Z]*[0-9]*[a-zA-Z]*\>', '', sentence) for sentence in com_removed]

    # Remove hexdump command
    hexdump_removed = [re.sub(r'\<[fF]+[0-9]+\>', '', sentence) for sentence in unicode_removed]

    # Remove the rest of unicode containing '<characters>m' or '< >'
    rest_removed = [re.sub(r'\<[a-zA-Z]*[0-9]*\>', '', sentence) for sentence in hexdump_removed]

    emoji_removed = [re.sub(r'\<U\S+\>', '', sentence) for sentence in rest_removed]


    # Remove weird ellipsis following after a character or string. Ex: än… => än
    ellipsis_removed = [re.sub(r'\…', '', sentence) for sentence in emoji_removed]


    # Remove text characters of HTML. Ex: &gt, &lt, &amp 
    html_removed = [re.sub(r'\&[gtampl]+', '', sentence) for sentence in ellipsis_removed]

    # Remove hyphen between strings. Ex prao-elev => prao, elev
    hyphen_removed = [re.sub(r'\-', '', sentence) for sentence in html_removed]

    # Remove plus between strings. Ex s+mp => s, mp
    plus_removed = [re.sub(r'\+', '', sentence) for sentence in hyphen_removed]

    # Remove slash between strings. Ex. rattfylleri/olovlig => rattfylleri olovlig
    slash_removed = [re.sub(r'\/', '', sentence) for sentence in plus_removed]

    # Make word-tokens
    words = make_words(slash_removed)


    
    # Remove punctuation
    words = [[w for w in subwords if w not in punctuation_tokens] for subwords in words]

    # Remove digits
    words = [[w for w in subwords if not w.isdigit()] for subwords in words]

    #stemmer = SnowballStemmer('swedish')
    #words  = [[stemmer.stem(w) for w in subwords] for subwords in words]

    stop_words = make_sw_stop_words()
    en_stop_words = make_en_stop_words()

    words = [[w for w in subwords if w not in stop_words] for subwords in words]

    words = [[w for w in subwords if w not in en_stop_words] for subwords in words]

    # Remove empty lists
    words = [w for w in words if w != []]

    return words
   


def flatten_hashtag_list(src):
    stop_words = make_sw_stop_words()
    stop_words_eng = make_en_stop_words()

    hashtag_data = pd.read_csv(src)
    hashtags = hashtag_data['hashtags']
    
    # Split the hashtags like ['hashtag', 'hashtag hashtag hashtag', ...] to [['hashtag'], ['hashtag', 'hashtag', 'hashtag'], ...]
    splitted = [t.split() for t in hashtags]

    # Flatten 'splitted' to one list
    flattened = []
    for sublist in splitted:
        for h in sublist:
            #h = re.sub(r'[0-9]', '', h)
            h = re.sub(r'\<[u]\+\S*\>', '', h)
            if (h not in stop_words) and (h not in stop_words_eng):
                flattened.append(h)
    return flattened

def unique_hashtag_list (src):
    hashtag_list = flatten_hashtag_list(src)
    unique_hashtag_list = list(set(hashtag_list))
    return unique_hashtag_list

