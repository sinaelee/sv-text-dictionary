#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd  
from pandas import DataFrame
import numpy as np

import csv
import preprocess as pre 

import gensim
from gensim.models import Word2Vec
from gensim.models import KeyedVectors


import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn import cluster
from sklearn import metrics
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import KMeans


import os
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
import sys
from time import time  # To time our operations

import nltk


def sort_by_hashtag():
    df = pd.read_csv('../word2vec/data/raw/hashtags_only.csv')
    #sorting data frame by Hashtag
    sorted_by_hashtag = df.sort_values(by=['hashtags']) 
    new_df = pd.DataFrame(sorted_by_hashtag)
    new_df.to_csv('../word2vec/data/tiddy/sorted_by_hashtag.csv')


# csv file for hashtag frequency : use 'hashtags_only.csv'
def get_hashtag_frequency(src):
    hashtags = pre.flatten_hashtag_list(src)
    frequency_distribution = nltk.FreqDist(hashtags)
    result = pd.DataFrame(list(frequency_distribution.items()), columns=['hashtags', 'frequency'])
    df = result.sort_values(by=['frequency'], ascending=False)
    df = df.reset_index(drop=True)
    df.index += 1
    df.to_csv('../word2vec/data/tiddy/hashtag_frequency.csv')


#sort_by_hashtag()
#get_hashtag_frequency('../copy_src/data/raw/hashtags_only.csv')


#df = pd.read_csv('../copy_src/data/raw/hashtags_only.csv')
#hashtags = df['hashtags']
#freq = df['frequency']
#similar_words = df['topn']  # a list of tuples [('w', cosine similarity), ...]

#pretrained = Word2Vec.load('w2v.model')

import ast
#df = similar_words=df['topn']
def similar_words_list(df):
    df_to_lst = df.values.tolist()
    similar_words_cosim = [ast.literal_eval(elem) for elem in df_to_lst]
    similar_words = [[x[0] for x in sublist] for sublist in similar_words_cosim]
    return similar_words

#similar = similar_words_list(similar_words)

def flat_lst(similar_ws):
    flat = [w for sublist in similar_ws for w in sublist]
    return flat
