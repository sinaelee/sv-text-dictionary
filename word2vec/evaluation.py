#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd  
from pandas import DataFrame

import csv

import gensim
from gensim.models import Word2Vec
from gensim.models import KeyedVectors

import utils as utils

import os
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
import sys
from time import time 
from pprint import pprint

def get_most_similar_for_csvfiles(model, src, path, topn):

    amount = 0
    df = pd.read_csv(src)
    unique_hashtags = df['hashtags']
    frequency = df['frequency']
    zipped = zip(unique_hashtags, frequency)
    hashtag_freq = dict(zipped)  

    #out_dir = 'results/cbow_expansion'
    #fname = str(topn) + '-similar_words_new' + '.csv'
    #out_path = os.path.join(out_dir, fname)
    with open(path, 'w') as csvfile:
        fieldnames = ['hashtags', 'frequency', 'topn', 'label']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        empty = ' '
        for h, f in hashtag_freq.items():
            if (h in model.wv.vocab) and (f > 50) :
                best_matches = model.wv.most_similar(positive=h, topn=topn)
                for x in best_matches:
                    writer.writerow({'hashtags': h, 'frequency':f, 'topn': x[0], 'label': empty})
            else:
                amount += 1



def get_most_similar(model, src, path, topn):

    amount = 0
    df = pd.read_csv('data/tiddy/hashtag_frequency.csv')
    unique_hashtags = df['hashtags']
    frequency = df['frequency']
    zipped = zip(unique_hashtags, frequency)
    hashtag_freq = dict(zipped)

    with open(path, 'w') as csvfile:
        fieldnames = ['hashtags', 'frequency', 'topn']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for h, f in hashtag_freq.items():
            if h in model.wv.vocab:
                best_matches = model.wv.most_similar(positive=h, topn=topn)
                writer.writerow({'hashtags': h, 'frequency':f, 'topn': [x[0] for x in best_matches]})
            else:
                amount += 1



cbow_in_dir = 'pretrained_models/cbow'
sg_in_dir = 'pretrained_models/sg'

cbow_out_dir = 'results/cbow_expansion'
sg_out_dir = 'results/sg_expansion'

csv_out = 'results/csv_for_next_project/frequency_50'

# Usage: python3 evaluation.py cbow 5
#        python3 evaluation.py sg 5
#        python3 evaluation.py csv 5
if __name__ == "__main__":
    if sys.argv[1] == 'cbow':
        cbow_file_lst = [f for f in os.listdir(cbow_in_dir) if f.endswith('.model')]
        cbow_file_lst.sort()
        cbow_file_lst = [os.path.join(cbow_in_dir, name) for name in cbow_file_lst]

        for cbow in cbow_file_lst:
            pretrained = Word2Vec.load(cbow)
            print ("Process file {0}...".format(cbow))

            model_name = os.path.splitext(os.path.basename(cbow))[0]
            fname = str(sys.argv[2]) + '-similar_words' + '.csv'
            out_path = os.path.join(cbow_out_dir, model_name + '-' + fname)
            
            get_most_similar(pretrained, 'data/tiddy/hashtag_frequency.csv', out_path, int(sys.argv[2]))


    elif sys.argv[1] == 'sg':
        sg_file_lst = [f for f in os.listdir(sg_in_dir) if f.endswith('.model')]
        sg_file_lst.sort()
        sg_file_lst = [os.path.join(sg_in_dir, name) for name in sg_file_lst]

        for sg in sg_file_lst:
            pretrained = Word2Vec.load(sg)
            print ("Process file {0}...".format(sg))

            model_name = os.path.splitext(os.path.basename(sg))[0]
            fname = str(sys.argv[2]) + '-similar_words' + '.csv'
            out_path = os.path.join(sg_out_dir, model_name + '-' + fname)

            get_most_similar(pretrained, 'data/tiddy/hashtag_frequency.csv', out_path, int(sys.argv[2]))

    elif sys.argv[1] == 'csv':
        cbow_file_lst = [f for f in os.listdir(cbow_in_dir) if f.endswith('.model')]
        cbow_file_lst.sort()
        cbow_file_lst = [os.path.join(cbow_in_dir, name) for name in cbow_file_lst]

        sg_file_lst = [f for f in os.listdir(sg_in_dir) if f.endswith('.model')]
        sg_file_lst.sort()
        sg_file_lst = [os.path.join(sg_in_dir, name) for name in sg_file_lst]

        for cbow in cbow_file_lst:
            pretrained =Word2Vec.load(cbow)
            print ("Process file {0}...".format(cbow))

            model_name = os.path.splitext(os.path.basename(cbow))[0]
            fname = str(sys.argv[2]) + '-similar_words' + '.csv'
            out_path = os.path.join(csv_out, model_name + '-' + fname)

            get_most_similar_for_csvfiles(pretrained, 'data/tiddy/hashtag_frequency.csv', out_path, int(sys.argv[2]))

        for sg in sg_file_lst:
            pretrained = Word2Vec.load(sg)
            print ("Process file {0}...".format(sg))

            model_name = os.path.splitext(os.path.basename(sg))[0]
            fname = str(sys.argv[2]) + '-similar_words' + '.csv'
            out_path = os.path.join(csv_out, model_name + '-' + fname)

            get_most_similar_for_csvfiles(pretrained, 'data/tiddy/hashtag_frequency.csv', out_path, int(sys.argv[2]))


    else:
        print("Usage: first parameter is cbow or sg, second parameter is number of similar words")

