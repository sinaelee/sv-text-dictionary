#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd  
from pandas import DataFrame

import csv
import preprocess as pre 

import gensim
from gensim.models import Word2Vec
from gensim.models import KeyedVectors

import os
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
import sys
from time import time

# read from csv-file
df1 = pd.read_csv('../word2vec/data/raw/hashtags_only.csv') # For training word2vec
tweets = df1['text']


t = time()
processed = pre.preprocess(tweets)
print('Time to pre-process tweets: {} mins'.format(round((time() - t) / 60, 2)))

def cbow(dim, it):
    fname = 'cbow-' + str(dim) + '-iter' + str(it) + '.model'
    current_model = Word2Vec(processed,  sg=0, size=dim, window=5, min_count=1, workers=4, hs=0, negative=5, iter=it)
    current_model.save(os.path.join('pretrained_models/cbow', fname))
   

def sg(dim, it):
    fname = 'sg-' + str(dim) + '-iter' + str(it) + '.model'
    current_model = Word2Vec(processed, sg=1, size=dim, window=5, min_count=1, workers=4, hs=0, negative=5, iter=it)
    current_model.save(os.path.join('pretrained_models/sg', fname))

#method (cbow or sgns), dimensionality, iter 
# Example: python3 train.py cbow 50 5
if __name__ == "__main__":
    if sys.argv[1] == "cbow":
        cbow(int(sys.argv[2]), int(sys.argv[3]))
    elif sys.argv[1] == "sg":
        sg(int(sys.argv[2]), int(sys.argv[3]))
    else:
    	print("First parameter: 'cbow' or 'sg'")

